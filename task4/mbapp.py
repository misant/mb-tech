import os
import time
import re
from slackclient import SlackClient
import boto3


# instantiate Slack client
# execute in cli:
# export SLACK_BOT_TOKEN='your bot user access token here'
slack_client = SlackClient(os.environ.get('SLACK_BOT_TOKEN'))

# bot's user ID in Slack: value is assigned after the bot starts up
bot_id = None

# constants
RTM_READ_DELAY = 1 # 1 second delay between reading from RTM
R_COMMAND = "list" # known received command
MENTION_REGEX = "^<@(|[WU].+?)>(.*)"


def parse_bot_commands(slack_events):
    """
        Parses a list of events coming from the Slack RTM API to find bot commands.
        If a bot command is found, this function returns a tuple of command and channel.
        If its not found, then this function returns None, None.
    """
    for event in slack_events:
        if event["type"] == "message" and not "subtype" in event:
            user_id, message = parse_direct_mention(event["text"])
            if user_id == bot_id:
                return message, event["channel"]
    return None, None

def parse_direct_mention(message_text):
    """
        Finds a direct mention (a mention that is at the beginning) in message text
        and returns the user ID which was mentioned. If there is no direct mention, returns None
    """
    matches = re.search(MENTION_REGEX, message_text)
    # the first group contains the username, the second group contains the remaining message
    return (matches.group(1), matches.group(2).strip()) if matches else (None, None)

def handle_command(command, channel):
    """
        Executes bot command if the command is known
    """
    # Default response is help text for the user
    default_response = "Invalid comand. Try *{}* [project,environment].".format(R_COMMAND)

    # Finds and executes the given command, filling in response
    response = None
    if command.startswith(R_COMMAND):
	if '[' in command and ']' in command:
    	    params = command.strip(R_COMMAND)
	    try:
		project, environment = re.sub('[^A-Za-z0-9,]+', '', params).split(',')
	    except ValueError:
		response = "Incorrect input. " + default_response 
	    try:
    		response = list_ips(project, environment)
	    except UnboundLocalError:
		response = "Incorrect input. " + default_response 
	    except:
		response = "Something went wrong getting details from AWS"

    # Sends the response back to the channel
    slack_client.api_call(
        "chat.postMessage",
        channel=channel,
        text=response or default_response
    )

def list_ips(project='', environment=''):
    """
        Get instances from AWS EC2 and if instance has both tags "project" and "environment" equal to reqested values
	returns instance details winth private and public IP addresses
    """

    ec2 = boto3.resource('ec2')
    output = ''
    for i in ec2.instances.all():
	if i.tags: # if not tags assigned to instanse do nothing
	    flag = 0
	    for t in i.tags: #check if both tags are there 
		if t['Key'] == 'project' and t['Value'] == project:
    		    flag += 1
		if t['Key'] == 'environment' and t['Value'] == environment:
    		    flag += 1

		if flag == 2:
		    output += "Id: {0}\t (*Priv. IP: {1}, Pub. IP: {2}*) \tState: {3}, Launched: {4}\n".format(i.id, i.private_ip_address, i.public_ip_address, i.state['Name'], i.launch_time)
    if not output:
	output = 'No instances with tags "project:{}" and "environment:{}" found.'.format(project, environment)
    return output

if __name__ == "__main__":
    if slack_client.rtm_connect(with_team_state=False):
        print("Bot connected and running!")
        # Read bot's user ID by calling Web API method `auth.test`
        bot_id = slack_client.api_call("auth.test")["user_id"]
        while True:
            command, channel = parse_bot_commands(slack_client.rtm_read())
            if command:
                handle_command(command, channel)
            time.sleep(RTM_READ_DELAY)
    else:
        print("Connection failed. Exception traceback printed above.")
